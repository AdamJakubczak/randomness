import requests

API_URL = 'https://api.quantumnumbers.anu.edu.au'
API_KEY = ''

length = 1

headers = {'x-api-key': API_KEY}
params = {
    'length' : length,
    'type' : 'uint16',
    }

response = requests.get(API_URL, params=params, headers=headers)
data = response.json()['data']

print(data)